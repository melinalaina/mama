/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio2;
import java.util.ArrayList;
/**
 *
 * @author pc
 */

public class Ejercicio2 {

    private ArrayList<Object> pila=new ArrayList();
    
    public void push(Object A){
        pila.add(A);
    }
    public Object pop(){
        if(!(pila.isEmpty())){
            Object A=pila.get(pila.size()-1);
            pila.remove(pila.size()-1);
            return A;
           
        }
        else{
            return null;
            
        }
    }
    public Object peek(){
        if(!(pila.isEmpty())){
          return pila.get(pila.size()-1);
        }
        else{
            return null;
        }
    }
    public boolean empty(){
        if(!(pila.isEmpty())){
            return false;
        }
        else{
            return true;
        }
    }
    public static void main(String[] args) {
       Ejercicio2 filo=new Ejercicio2();
       filo.push(4.3);
       filo.push(true);
       filo.push("pila");
       System.out.println(filo.pila);
       System.out.println(filo.pop());
       System.out.println(filo.pop());
       System.out.println(filo.pop());
       System.out.println(filo.peek());
       System.out.println("la pila esta vacia"+filo.empty());
      }
}
